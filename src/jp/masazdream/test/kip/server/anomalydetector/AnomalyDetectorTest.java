package jp.masazdream.test.kip.server.anomalydetector;

import static org.junit.Assert.*;
import jp.masazdream.kip.server.anomalydetector.AnomalyDetectorClient;
import jp.masazdream.kip.server.anomalydetector.Features;
import jp.masazdream.kip.server.anomalydetector.Metrics;
import jp.masazdream.kip.server.anomalydetector.statics.Statics;
import jp.masazdream.kip.server.anomalydetector.util.FileDataUtil;

import org.junit.Test;

public class AnomalyDetectorTest {
	AnomalyDetectorClient detector = new AnomalyDetectorClient();

	@Test
	public void collectFeaturesTest() {
		for (int i = 0; i < 100; i++) {
			Features features = detector.collectFeatures();
			FileDataUtil
					.saveToFile(
							"C:\\workspace\\KnowledgeInformationProcessing\\features\\serverDataFeatures.tsv",
							features);

			try {
				Thread.sleep(1000 * 60 * 5);
			} catch (InterruptedException e) {
			}
		}
	}

	@Test
	public void cpuMetricTest() {

		Metrics metrics = detector.getMetrics(Statics.CPU_METRIC_NAME_SPACE,
				Statics.CPU_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void diskReadTest() {

		Metrics metrics = detector.getMetrics(
				Statics.DISK_READ_METRIC_NAME_SPACE,
				Statics.DISK_READ_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void diskWriteTest() {

		Metrics metrics = detector.getMetrics(
				Statics.DISK_WRITE_METRIC_NAME_SPACE,
				Statics.DISK_WRITE_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void networkInTest() {

		Metrics metrics = detector.getMetrics(
				Statics.NETWORK_IN_METRIC_NAME_SPACE,
				Statics.NETWORK_IN_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void networkOutTest() {
		AnomalyDetectorClient detector = new AnomalyDetectorClient();

		Metrics metrics = detector.getMetrics(
				Statics.NETWORK_OUT_METRIC_NAME_SPACE,
				Statics.NETWORK_OUT_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void diskUtilTest() {
		AnomalyDetectorClient detector = new AnomalyDetectorClient();

		Metrics metrics = detector.getMetrics(
				Statics.DISK_SPACE_METRIC_NAME_SPACE,
				Statics.DISK_SPACE_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void memotyUtilTest() {
		AnomalyDetectorClient detector = new AnomalyDetectorClient();

		Metrics metrics = detector.getMetrics(
				Statics.MEMORY_UTILIZATION_METRIC_NAME_SPACE,
				Statics.MEMORY_UTILIZATION_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

	@Test
	public void swapUtilTest() {
		AnomalyDetectorClient detector = new AnomalyDetectorClient();

		Metrics metrics = detector.getMetrics(
				Statics.SWAP_UTILIZATION_METRIC_NAME_SPACE,
				Statics.SWAP_UTILIZATION_METRIC_NAME);

		detector.getMetricsStatistics(metrics.getNameSpace(),
				metrics.getMetricName(), metrics.getDimensions());
	}

}
