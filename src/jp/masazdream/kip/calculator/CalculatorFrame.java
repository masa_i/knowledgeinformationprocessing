package jp.masazdream.kip.calculator;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class CalculatorFrame extends Frame implements ActionListener, WindowListener  {
	public static final String ERROR = "error";

	private Label mText;
	private Button mButton[];
	private Panel mNumberPanel;
	private Button mClear;
	private Button mPlus;
	private Button mMinus;
	private Button mEqual;
	private Button mSqrt;
	private Button mSin;
	private Button mCos;

	private Panel mCommandPanel;
	private String mBuffer;
	private int mResult;
	private String mDisp;
	private String mOperator;
	private boolean mAppend;

	public CalculatorFrame() {
		setTitle("Calculator");

		initBuffer();
		initOperator();
		mAppend = false;
		mText = new Label(mBuffer, Label.RIGHT);
		mText.setBackground(Color.white);
		showBuffer();
		mResult = Integer.parseInt(mBuffer);

		mButton = new Button[10];
		for (int i = 0; i < 10; i++) {
			// ActionCommand
			mButton[i] = new Button((new Integer(i)).toString());
			mButton[i].addActionListener(this);
		}

		mClear = new Button("C");
		mClear.addActionListener(this);
		mPlus = new Button("+");
		mPlus.addActionListener(this);
		mMinus = new Button("-");
		mMinus.addActionListener(this);
		mEqual = new Button("=");
		mEqual.addActionListener(this);
		mSqrt = new Button("√");
		mSqrt.addActionListener(this);
		mSin = new Button("sin");
		mSin.addActionListener(this);
		mCos = new Button("cos");
		mCos.addActionListener(this);

		mNumberPanel = new Panel();
		mNumberPanel.setLayout(new GridLayout(4, 3));
		for (int i = 1; i < 10; i++) {
			mNumberPanel.add(mButton[i]);
		}
		mNumberPanel.add(mButton[0]);

		mCommandPanel = new Panel();
		mCommandPanel.setLayout(new GridLayout(4, 2));
		mCommandPanel.add(mClear);
		mCommandPanel.add(mSqrt);
		mCommandPanel.add(mPlus);
		mCommandPanel.add(mSin);
		mCommandPanel.add(mMinus);
		mCommandPanel.add(mCos);
		mCommandPanel.add(mEqual);

		setLayout(new BorderLayout());
		add("North", mText);
		add("Center", mNumberPanel);
		add("East", mCommandPanel);
		pack();

		addWindowListener(this);
	}

	private void showBuffer() {
		try{
			mBuffer = Integer.toString(Integer.parseInt(mBuffer));
		}catch(NumberFormatException e){
			mBuffer = Double.toString(Double.parseDouble(mBuffer));
		}
		mText.setText(mBuffer);
	}

	private void initBuffer() {
		mBuffer = null;
		mBuffer = new String("0");
	}

	private void initOperator() {
		mOperator = null;
		mOperator = new String("none");
	}

	private void setOperator(String theOperator) {
		mOperator = theOperator;
	}

	private void calculate() {
		int mBufferInt = 0;
		try {
			mBufferInt = Integer.parseInt(mBuffer);
		} catch (NumberFormatException e) {
			// 数字以外の例外
			mText.setText(ERROR);
			return;
		}
		if (mOperator.equals("plus")) {
			mResult = mResult + mBufferInt;
			mBuffer = Integer.toString(mResult);
			showBuffer();
		} else if (mOperator.equals("minus")) {
			mResult = mResult - mBufferInt;
			mBuffer = Integer.toString(mResult);
			showBuffer();
		} else {
			mResult = mBufferInt;
		}
	}
	
	private void calculate(String type){
		double buffer = Double.parseDouble(mBuffer);
		double result = 0;
		if (type.equals("sqrt")) {
			result = Math.sqrt(buffer);
		} else if (type.equals("sin")) {
			double radian = Math.toRadians(buffer);
			result = Math.sin(radian);
		} else if (type.equals("cos")) {
			double radian = Math.toRadians(buffer);
			result = Math.cos(radian);
		}
		mBuffer = Double.toString(result);
		showBuffer();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if (source == mClear) {
			// クリアの場合
			initBuffer();
			initOperator();
			showBuffer();
		} else if (source == mPlus) {
			// プラスの場合
			calculate();
			setOperator("plus");
			mAppend = false;
		} else if (source == mMinus) {
			// マイナスの場合
			calculate();
			setOperator("minus");
			mAppend = false;
		} else if (source == mEqual) {
			// イコールの場合
			calculate();
			mAppend = false;
			initOperator();
		} else if (source == mSqrt) {
			calculate("sqrt");
			mAppend = false;
			initOperator();
		} else if (source == mSin) {
			calculate("sin");
			mAppend = false;
			initOperator();
		} else if (source == mCos) {
			calculate("cos");
			mAppend = false;
			initOperator();
		} else {
			// 0 ～ 9の数字の場合
			if (mAppend) {
				// eventのactionCommandには
				mBuffer = mBuffer + event.getActionCommand();
			} else {
				mBuffer = event.getActionCommand();
			}
			mAppend = true;
			showBuffer();
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
	}
}
