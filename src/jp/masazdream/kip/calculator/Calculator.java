package jp.masazdream.kip.calculator;

/**
 * 電卓クラス
 * 
 * @author masahiro
 *
 */
public class Calculator {
	public static CalculatorFrame mCalculatorFrame;
	
	public static void main(String[] args){
		mCalculatorFrame = new CalculatorFrame();
		mCalculatorFrame.setVisible(true);
		
	}
}
