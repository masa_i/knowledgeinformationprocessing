package jp.masazdream.kip.search;

import java.util.ArrayList;

import jp.masazdream.kip.util.FileUtils;

/**
 * 状態空間クラス
 * 
 * ノードとリンクを保持する
 * 
 * @author masahiro
 * 
 */
public class StateSpace {
	/**
	 * ノードリスト
	 */
	ArrayList<Node> mNodeList = null;

	/**
	 * リンクリスト
	 */
	ArrayList<Link> mLinkList = null;

	/**
	 * コンストラクタ
	 * 
	 * 与えられたファイルから状態空間を作成する
	 * 
	 * @param filePath
	 *            　ファイル
	 * @throws Exception
	 */
	public StateSpace(String filePath) throws Exception {
		mNodeList = new ArrayList<Node>();
		mLinkList = new ArrayList<Link>();
		ArrayList<String> dataLines = FileUtils.loadFile(filePath);

		ArrayList<String> nodeNameList = new ArrayList<>();

		boolean isNode = false;
		boolean isLink = false;
		for (String line : dataLines) {
			if (line.equals("#Node")) {
				isNode = true;
				isLink = false;
				continue;
			} else if (line.equals("#Link")) {
				isNode = false;
				isLink = true;
				continue;
			}

			String[] lineElements = line.split("\t");

			if (isNode) {
				// Nodeの場合
				if (lineElements.length < 2) {
					// Nodeとして不正
					throw new Exception("[Error] failed node. " + line);
				}
				String nodeName = lineElements[0];
				String hValueStr = lineElements[1];

				// Nodeの作成
				try {
					int hValue = Integer.parseInt(hValueStr);
					mNodeList.add(new Node(nodeName, hValue));
				} catch (Exception e) {
					throw new Exception("[Error] failed h value. " + line);
				}

				// Nodeの名前を保存
				for (String name : nodeNameList) {
					if (name.equals(nodeName)) {
						// 同じ名前が含まれている場合グラフが壊れる
						throw new Exception("[Error] same node name. " + line);
					}
				}
				// ノードに名前を追加
				nodeNameList.add(nodeName);
			} else if (isLink) {
				// Linkの場合
				if (lineElements.length < 4) {
					// Linkとして不正
					throw new Exception("[Error] failed link. " + line);
				}
				String startNodeName = lineElements[0];
				String endNodeName = lineElements[1];
				String linkTypeStr = lineElements[2];
				String costStr = lineElements[3];

				try {
					boolean isDirect = false;
					if (linkTypeStr.equals("d")) {
						isDirect = true;
					} else if (linkTypeStr.equals("u")) {
						isDirect = false;
					} else {
						throw new Exception("failed link type. ");
					}
					int cost = Integer.parseInt(costStr);
					;

					mLinkList.add(new Link(startNodeName, endNodeName,
							isDirect, cost));
				} catch (Exception e) {
					throw new Exception(e.getMessage() + line);
				}
			}
		}
	}

	public ArrayList<Node> getmNodeList() {
		return mNodeList;
	}

	public void setmNodeList(ArrayList<Node> mNodeList) {
		this.mNodeList = mNodeList;
	}

	public ArrayList<Link> getmLinkList() {
		return mLinkList;
	}

	public void setmLinkList(ArrayList<Link> mLinkList) {
		this.mLinkList = mLinkList;
	}
}

// テキストの状態空間

