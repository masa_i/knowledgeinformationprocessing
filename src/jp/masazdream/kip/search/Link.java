package jp.masazdream.kip.search;

/**
 * リンクオブジェクト
 * 
 * @author masahiro
 *
 */
public class Link {
	public String mStartNodeName;
	
	public String mEndNodeName;
	
	public boolean mIsDirect;
	
	public int mCost;
	
	public Link(String startNodeName, String endNodeName, boolean isDirect, int cost){
		mStartNodeName = startNodeName;
		mEndNodeName = endNodeName;
		mIsDirect = isDirect;
		mCost = cost;
	}

	public String getmStartNodeName() {
		return mStartNodeName;
	}

	public void setmStartNodeName(String mStartNodeName) {
		this.mStartNodeName = mStartNodeName;
	}

	public String getmEndNodeName() {
		return mEndNodeName;
	}

	public void setmEndNodeName(String mEndNodeName) {
		this.mEndNodeName = mEndNodeName;
	}

	public boolean ismIsDirect() {
		return mIsDirect;
	}

	public void setmIsDirect(boolean mIsDirect) {
		this.mIsDirect = mIsDirect;
	}

	public int getmCost() {
		return mCost;
	}

	public void setmCost(int mCost) {
		this.mCost = mCost;
	}
	
	
}
