package jp.masazdream.kip.search;

import java.awt.Color;
import java.util.*;

import javax.swing.JFrame;

import jp.masazdream.kip.graph.GraphLapper;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.EdgeLabelRenderer;
import edu.uci.ics.jung.visualization.renderers.Renderer.Edge;
import edu.uci.ics.jung.visualization.renderers.VertexLabelRenderer;

public class Search {
	Node node[];
	Node goal;
	Node start;
	StateSpace mStateSpace = null;

	/**
	 * コンストラクタ
	 * 
	 * @param filePath
	 *            　データを読み込むファイルパス
	 * @throws Exception 
	 */
	public Search(String filePath) throws Exception {
		makeStateSpace(filePath);
	}

	/**
	 * 状態空間を作成する
	 * 
	 * @param filePath
	 */
	private void makeStateSpace(String filePath) throws Exception{
		try {
			mStateSpace = new StateSpace(filePath);
		} catch (Exception e) {
			// 例外が発生した場合
			System.out.println(e.getMessage());
			return;
		}
		// 空間を配列に並べる
		// ノード
		ArrayList<Node> nodes = mStateSpace.getmNodeList();
		int nodeSize = nodes.size();
		node = new Node[nodeSize];
		HashMap<String, Integer> nodeNameIndexMap = new HashMap<>();
		for (int i = 0; i < nodeSize; i++) {
			Node n = nodes.get(i);
			node[i] = n;
			nodeNameIndexMap.put(n.getName(), i);
		}
		start = node[0];
		goal = node[nodeSize - 1];

		// リンク
		ArrayList<Link> links = mStateSpace.getmLinkList();
		for (Link link : links) {
			String startNodeName = link.getmStartNodeName();
			String endNodeName = link.getmEndNodeName();
			int cost = link.getmCost();

			if (nodeNameIndexMap.containsKey(startNodeName)
					&& nodeNameIndexMap.containsKey(endNodeName)) {
				int startNodeIndex = nodeNameIndexMap.get(startNodeName);
				int endNodeIndex = nodeNameIndexMap.get(endNodeName);

				// 親ノード(start)に子ノード(end)を追加する
				node[startNodeIndex].addChild(node[endNodeIndex], cost);
			}else{
				throw new Exception("[Error] failed to link node." + startNodeName + "->" + endNodeName);	
			}
		}
	}

	/***
	 * 幅優先探索
	 */
	public void breadthFirst() {
		Vector open = new Vector();
		open.addElement(node[0]);
		Vector closed = new Vector();
		boolean success = false;
		int step = 0;

		for (;;) {
			System.out.println("STEP:" + (step++));
			System.out.println("OPEN:" + open.toString());
			System.out.println("CLOSED:" + closed.toString());
			// openは空か？
			if (open.size() == 0) {
				success = false;
				break;
			} else {
				// openの先頭を取り出し node とする．
				Node node = (Node) open.elementAt(0);
				open.removeElementAt(0);
				// node は ゴールか？
				if (node == goal) {
					success = true;
					break;
				} else {
					// node を展開して子節点をすべて求める．
					Vector children = node.getChildren();
					// node を closed に入れる．
					closed.addElement(node);
					// 子節点 m が open にも closed にも含まれていなければ，
					for (int i = 0; i < children.size(); i++) {
						Node m = (Node) children.elementAt(i);
						if (!open.contains(m) && !closed.contains(m)) {
							// m から node へのポインタを付ける．
							m.setPointer(node);
							if (m == goal) {
								open.insertElementAt(m, 0);
							} else {
								open.addElement(m);
							}
						}
					}
				}
			}
		}
		if (success) {
			System.out.println("*** Solution ***");
			printSolution(goal);
		}
	}

	/***
	 * 深さ優先探索
	 */
	public void depthFirst() {
		Vector open = new Vector();
		open.addElement(node[0]);
		Vector closed = new Vector();
		boolean success = false;
		int step = 0;

		for (;;) {
			System.out.println("STEP:" + (step++));
			System.out.println("OPEN:" + open.toString());
			System.out.println("CLOSED:" + closed.toString());
			// openは空か？
			if (open.size() == 0) {
				success = false;
				break;
			} else {
				// openの先頭を取り出し node とする．
				Node node = (Node) open.elementAt(0);
				open.removeElementAt(0);
				// node は ゴールか？
				if (node == goal) {
					success = true;
					break;
				} else {
					// node を展開して子節点をすべて求める．
					Vector children = node.getChildren();
					// node を closed に入れる．
					closed.addElement(node);
					// 子節点 m が open にも closed にも含まれていなければ，
					// 以下を実行．幅優先探索と異なるのはこの部分である．
					// j は複数の子節点を適切にopenの先頭に置くために位置
					// を調整する変数であり，一般には展開したときの子節点
					// の位置は任意でかまわない．
					int j = 0;
					for (int i = 0; i < children.size(); i++) {
						Node m = (Node) children.elementAt(i);
						if (!open.contains(m) && !closed.contains(m)) {
							// m から node へのポインタを付ける
							m.setPointer(node);
							if (m == goal) {
								open.insertElementAt(m, 0);
							} else {
								open.insertElementAt(m, j);
							}
							j++;
						}
					}
				}
			}
		}
		if (success) {
			System.out.println("*** Solution ***");
			printSolution(goal);
		}
	}

	/***
	 * 分岐限定法
	 */
	public void branchAndBound() {
		Vector open = new Vector();
		open.addElement(start);
		start.setGValue(0);
		Vector closed = new Vector();
		boolean success = false;
		int step = 0;

		for (;;) {
			System.out.println("STEP:" + (step++));
			System.out.println("OPEN:" + open.toString());
			System.out.println("CLOSED:" + closed.toString());
			// openは空か？
			if (open.size() == 0) {
				success = false;
				break;
			} else {
				// openの先頭を取り出し node とする．
				Node node = (Node) open.elementAt(0);
				open.removeElementAt(0);
				// node は ゴールか？
				if (node == goal) {
					success = true;
					break;
				} else {
					// node を展開して子節点をすべて求める．
					Vector children = node.getChildren();
					// node を closed に入れる．
					closed.addElement(node);
					for (int i = 0; i < children.size(); i++) {
						Node m = (Node) children.elementAt(i);
						// 子節点mがopenにもclosedにも含まれていなければ，
						if (!open.contains(m) && !closed.contains(m)) {
							// m から node へのポインタを付ける．
							m.setPointer(node);
							// nodeまでの評価値とnode->mのコストを
							// 足したものをmの評価値とする
							int gmn = node.getGValue() + node.getCost(m);
							m.setGValue(gmn);
							open.addElement(m);
						}
						// 子節点mがopenに含まれているならば，
						if (open.contains(m)) {
							int gmn = node.getGValue() + node.getCost(m);
							if (gmn < m.getGValue()) {
								m.setGValue(gmn);
								m.setPointer(node);
							}
						}
					}
				}
			}
			open = sortUpperByGValue(open);
		}
		if (success) {
			System.out.println("*** Solution ***");
			printSolution(goal);
		}
	}

	/***
	 * 山登り法
	 */
	public void hillClimbing() {
		Vector open = new Vector();
		open.addElement(start);
		start.setGValue(0);
		Vector closed = new Vector();
		boolean success = false;

		// Start を node とする．
		Node node = start;
		for (;;) {
			// node は ゴールか？
			if (node == goal) {
				success = true;
				break;
			} else {
				// node を展開して子節点をすべて求める．
				Vector children = node.getChildren();
				System.out.println(children.toString());
				for (int i = 0; i < children.size(); i++) {
					Node m = (Node) children.elementAt(i);
					// m から node へのポインタを付ける．
					m.setPointer(node);
				}
				// 子節点の中に goal があれば goal を node とする．
				// なければ，最小の hValue を持つ子節点 m を node
				// とする．
				boolean goalp = false;
				Node min = (Node) children.elementAt(0);
				for (int i = 0; i < children.size(); i++) {
					Node a = (Node) children.elementAt(i);
					if (a == goal) {
						goalp = true;
					} else if (min.getHValue() > a.getHValue()) {
						min = a;
					}
				}
				if (goalp) {
					node = goal;
				} else {
					node = min;
				}
			}
		}
		if (success) {
			System.out.println("*** Solution ***");
			printSolution(goal);
		}
	}

	/***
	 * 最良優先探索
	 */
	public void bestFirst() {
		Vector open = new Vector();
		open.addElement(start);
		start.setGValue(0);
		Vector closed = new Vector();
		boolean success = false;
		int step = 0;

		for (;;) {
			System.out.println("STEP:" + (step++));
			System.out.println("OPEN:" + open.toString());
			System.out.println("CLOSED:" + closed.toString());
			// openは空か？
			if (open.size() == 0) {
				success = false;
				break;
			} else {
				// openの先頭を取り出し node とする．
				Node node = (Node) open.elementAt(0);
				open.removeElementAt(0);
				// node は ゴールか？
				if (node == goal) {
					success = true;
					break;
				} else {
					// node を展開して子節点をすべて求める．
					Vector children = node.getChildren();
					// node を closed に入れる．
					closed.addElement(node);
					for (int i = 0; i < children.size(); i++) {
						Node m = (Node) children.elementAt(i);
						// 子節点mがopenにもclosedにも含まれていなければ，
						if (!open.contains(m) && !closed.contains(m)) {
							// m から node へのポインタを付ける．
							m.setPointer(node);
							open.addElement(m);
						}
					}
				}
			}
			open = sortUpperByHValue(open);
		}
		if (success) {
			System.out.println("*** Solution ***");
			printSolution(goal);
		}
	}

	/***
	 * A* アルゴリズム
	 */
	public void aStar() {
		Vector open = new Vector();
		open.addElement(start);
		start.setGValue(0);
		start.setFValue(0);
		Vector closed = new Vector();
		boolean success = false;
		int step = 0;

		for (;;) {
			System.out.println("STEP:" + (step++));
			System.out.println("OPEN:" + open.toString());
			System.out.println("CLOSED:" + closed.toString());
			// openは空か？
			if (open.size() == 0) {
				success = false;
				break;
			} else {
				// openの先頭を取り出し node とする．
				Node node = (Node) open.elementAt(0);
				open.removeElementAt(0);
				// node は ゴールか？
				if (node == goal) {
					success = true;
					break;
				} else {
					// node を展開して子節点をすべて求める．
					Vector children = node.getChildren();
					// node を closed に入れる．
					closed.addElement(node);
					for (int i = 0; i < children.size(); i++) {
						Node m = (Node) children.elementAt(i);
						int gmn = node.getGValue() + node.getCost(m);
						int fmn = gmn + m.getHValue();

						// 各子節点mの評価値とポインタを設定する
						if (!open.contains(m) && !closed.contains(m)) {
							// 子節点mがopenにもclosedにも含まれていない場合
							// m から node へのポインタを付ける．
							m.setGValue(gmn);
							m.setFValue(fmn);
							m.setPointer(node);
							// mをopenに追加
							open.addElement(m);
						} else if (open.contains(m)) {
							// 子節点mがopenに含まれている場合
							if (gmn < m.getGValue()) {
								// 評価値を更新し，m から node へのポインタを付け替える
								m.setGValue(gmn);
								m.setFValue(fmn);
								m.setPointer(node);
							}
						} else if (closed.contains(m)) {
							if (gmn < m.getGValue()) {
								// 子節点mがclosedに含まれていて fmn < fm となる場合
								// 評価値を更新し，mからnodeへのポインタを付け替える
								m.setGValue(gmn);
								m.setFValue(fmn);
								m.setPointer(node);
								// 子節点mをclosedからopenに移動
								closed.removeElement(m);
								open.addElement(m);
							}
						}
					}
				}
			}
			open = sortUpperByFValue(open);
		}
		if (success) {
			System.out.println("*** Solution ***");
			printSolution(goal);
		}
	}

	/***
	 * 解の表示
	 * 
	 * ゴールノードのポインタをBackして表示する
	 */
	public void printSolution(Node theNode) {
		if (theNode == start) {
			System.out.println(theNode.toString());
		} else {
			System.out.print(theNode.toString() + " <- ");
			printSolution(theNode.getPointer());
		}
	}

	/***
	 * Vector を Node の fValue の昇順（小さい順）に列べ換える．
	 */
	public Vector sortUpperByFValue(Vector theOpen) {
		Vector newOpen = new Vector();
		Node min, tmp = null;
		while (theOpen.size() > 0) {
			min = (Node) theOpen.elementAt(0);
			for (int i = 1; i < theOpen.size(); i++) {
				tmp = (Node) theOpen.elementAt(i);
				if (min.getFValue() > tmp.getFValue()) {
					min = tmp;
				}
			}
			newOpen.addElement(min);
			theOpen.removeElement(min);
		}
		return newOpen;
	}

	/***
	 * Vector を Node の gValue の昇順（小さい順）に列べ換える．
	 */
	public Vector sortUpperByGValue(Vector theOpen) {
		Vector newOpen = new Vector();
		Node min, tmp = null;
		while (theOpen.size() > 0) {
			min = (Node) theOpen.elementAt(0);
			for (int i = 1; i < theOpen.size(); i++) {
				tmp = (Node) theOpen.elementAt(i);
				if (min.getGValue() > tmp.getGValue()) {
					min = tmp;
				}
			}
			newOpen.addElement(min);
			theOpen.removeElement(min);
		}
		return newOpen;
	}

	/***
	 * Vector を Node の hValue の昇順（小さい順）に列べ換える．
	 */
	public Vector sortUpperByHValue(Vector theOpen) {
		Vector newOpen = new Vector();
		Node min, tmp = null;
		while (theOpen.size() > 0) {
			min = (Node) theOpen.elementAt(0);
			for (int i = 1; i < theOpen.size(); i++) {
				tmp = (Node) theOpen.elementAt(i);
				if (min.getHValue() > tmp.getHValue()) {
					min = tmp;
				}
			}
			newOpen.addElement(min);
			theOpen.removeElement(min);
		}
		return newOpen;
	}

	public void draw() {
		// 表示フレーム
		JFrame jf = new JFrame();

		
		// グラフ
		GraphLapper gl = new GraphLapper();
		final Graph graph = gl.getSparseGraph();
		
		// Node
		ArrayList<Node> nodes = mStateSpace.getmNodeList();
		for(Node node : nodes){
			graph.addVertex(node.getName());
		}
		
		// Link
		final ArrayList<Link> links = mStateSpace.getmLinkList();
		int linkCnt = 1;
		for(Link link : links){
			graph.addEdge(linkCnt, link.getmStartNodeName(), link.getmEndNodeName(), EdgeType.DIRECTED);
			linkCnt++;
		}
		
		// 表示
		VisualizationViewer vv = new VisualizationViewer(new FRLayout(graph));
		VertexLabelRenderer vertexLabelRenderer = vv.getRenderContext()
				.getVertexLabelRenderer();

		// エッジの表示
		EdgeLabelRenderer edgeLabelRenderer = vv.getRenderContext()
				.getEdgeLabelRenderer();
		edgeLabelRenderer.setRotateEdgeLabels(true);

		// エッジの表示
		Transformer<Number, String> stringer = new Transformer<Number, String>() {
			public String transform(Number e) {
				int cnt = 1;
				for (Iterator edges = graph.getEdges().iterator(); edges.hasNext();) {
					if(cnt == e.intValue()){
						return Integer.toString(links.get(cnt - 1).getmCost());
					}
					cnt++;
				}
				return "no cost";
			}
		};

		// 表示設定セット
		// vv.getRenderContext().setEdgeDrawPaintTransformer(new
		// EdgeShape.Line());
		vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
		vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
		vv.getRenderContext().setEdgeLabelRenderer(edgeLabelRenderer);
		// add a listener for ToolTips
		vv.setVertexToolTipTransformer(new ToStringLabeller());
		vv.getRenderContext().setEdgeLabelTransformer(stringer);
		// vv.getRenderContext().setEdgeDrawPaintTransformer(new
		// PickableEdgePaintTransformer<Number>(vv.getPickedEdgeState(),
		// Color.black, Color.cyan));
		vv.getRenderContext().setArrowFillPaintTransformer(
				new ConstantTransformer(Color.lightGray));

		// フレームに追加
		jf.getContentPane().add(vv);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.pack();
		jf.setVisible(true);
	}
}
