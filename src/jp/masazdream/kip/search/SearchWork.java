package jp.masazdream.kip.search;

public class SearchWork {

	/**
	 * 探索起動クラス
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("USAGE:");
			System.out.println("java Search [Number] [Data filepath]");
			System.out.println("[Number] = 1 : Bredth First Search");
			System.out.println("[Number] = 2 : Depth  First Search");
			System.out.println("[Number] = 3 : Branch and Bound Search");
			System.out.println("[Number] = 4 : Hill Climbing Search");
			System.out.println("[Number] = 5 : Best First Search");
			System.out.println("[Number] = 6 : A star Algorithm");
			System.exit(-1);
		}

		// 検索の種類
		String searchType = args[0];
		String filePath = args[1];
		Search search = null;
		try {
			search = new Search(filePath);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		if(search == null){
			System.err.println("[Error] search is null.");
			System.exit(-1);
		}
		
		int type = Integer.parseInt(searchType);

		// 種類毎の検索
		switch (type) {
		case 1:
			// 幅優先探索
			System.out.println("\nBreadth First Search");
			search.breadthFirst();
			break;
		case 2:
			// 深さ優先探索
			System.out.println("\nDepth First Search");
			search.depthFirst();
			break;
		case 3:
			// 分岐限定法
			System.out.println("\nBranch and Bound Search");
			search.branchAndBound();
			break;
		case 4:
			// 山登り法
			System.out.println("\nHill Climbing Search");
			search.hillClimbing();
			break;
		case 5:
			// 最良優先探索
			System.out.println("\nBest First Search");
			search.bestFirst();
			break;
		case 6:
			// A*アルゴリズム
			System.out.println("\nA star Algorithm");
			search.aStar();
			break;
		default:
			System.out.println("Please input numbers 1 to 6");
		}
		
		search.draw();
	}
}
