package jp.masazdream.kip.statics;

/**
 * statics class
 * 
 * @author masahiro
 *
 */
public class Statics {
	public static final String KIP_PROCESS_START = "kip process start.";
}
