package jp.masazdream.kip.graph;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

public class GraphLapper {
	/**
	 * 空の疎グラフオブジェクトを返す
	 * 
	 * @return
	 */
	public SparseMultigraph<Integer, Number> getSparseGraph(){
		return new SparseMultigraph<Integer, Number>();
	}

	/**
	 * 無向グラフオブジェクト
	 * 
	 * @return
	 */
	public UndirectedSparseGraph<Integer, Number> getUndirectedSparseGraph(){
		return new UndirectedSparseGraph<Integer, Number>();
	}
	
	/**
	 * 有向グラフオブジェクト
	 * 
	 * @return
	 */
	public DirectedSparseGraph<Integer, Number> getDirectedSparseGraph(){
		return new DirectedSparseGraph<Integer, Number>();
	}
}
