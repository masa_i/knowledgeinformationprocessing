package jp.masazdream.kip;

import jp.masazdream.kip.statics.Statics;

/**
 * Engineering of Knowledge Information Processing 's code
 * 
 * masahiro imai
 */
public class KipMain {
	public static void main(String[] args){
		System.out.println(Statics.KIP_PROCESS_START);
	}
}
