package jp.masazdream.kip.server.anomalydetector;

public class Features {
	private double cpuUtil;

	private double diskRead;

	private double diskWrite;

	private double networkIn;

	private double networkOut;

	private double diskUtil;

	private double memoryUtil;

	private double swapUtil;

	// auto generate getter and setter

	public double getCpuUtil() {
		return cpuUtil;
	}

	public void setCpuUtil(double cpuUtil) {
		this.cpuUtil = cpuUtil;
	}

	public double getDiskRead() {
		return diskRead;
	}

	public void setDiskRead(double diskRead) {
		this.diskRead = diskRead;
	}

	public double getDiskWrite() {
		return diskWrite;
	}

	public void setDiskWrite(double diskWrite) {
		this.diskWrite = diskWrite;
	}

	public double getNetworkIn() {
		return networkIn;
	}

	public void setNetworkIn(double networkIn) {
		this.networkIn = networkIn;
	}

	public double getNetworkOut() {
		return networkOut;
	}

	public void setNetworkOut(double networkOut) {
		this.networkOut = networkOut;
	}

	public double getDiskUtil() {
		return diskUtil;
	}

	public void setDiskUtil(double diskUtil) {
		this.diskUtil = diskUtil;
	}

	public double getMemoryUtil() {
		return memoryUtil;
	}

	public void setMemoryUtil(double memoryUtil) {
		this.memoryUtil = memoryUtil;
	}

	public double getSwapUtil() {
		return swapUtil;
	}

	public void setSwapUtil(double swapUtil) {
		this.swapUtil = swapUtil;
	}

}
