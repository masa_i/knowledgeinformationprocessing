package jp.masazdream.kip.server.anomalydetector;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import jp.masazdream.kip.server.anomalydetector.statics.Statics;
import jp.masazdream.kip.server.anomalydetector.util.DataPointComparator;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.ListMetricsRequest;
import com.amazonaws.services.cloudwatch.model.ListMetricsResult;

/**
 * サーバのanomaly detectorクライアントクラス
 * 
 * @author masahiro
 *
 */
public class AnomalyDetectorClient {
	AWSCredentials mAwsCredentials;

	AmazonCloudWatchClient mClient;

	public AnomalyDetectorClient() {
		mAwsCredentials = new BasicAWSCredentials(Statics.ACCESS_KEY,
				Statics.SECRET_ACCESS_KEY);
		mClient = new AmazonCloudWatchClient(mAwsCredentials);
		mClient.setEndpoint(Statics.MONITORING_END_POINT);
	}

	// 検出処理の実行
	public Features collectFeatures() {
		Features features = new Features();
		int metricNum = Statics.METRIC_NAME_SPACES.length;
		for (int i = 0; i < metricNum - 1; i++) {
			Metrics metrics = getMetrics(Statics.METRIC_NAME_SPACES[i],
					Statics.METRIC_NAMES[i]);
			double data = getMetricsStatistics(metrics.getNameSpace(),
					metrics.getMetricName(), metrics.getDimensions());

			switch (i) {
			case Statics.CPU_METRIC_INDEX:
				features.setCpuUtil(data);
				break;
			case Statics.NETWORK_IN_INDEX:
				features.setNetworkIn(data);
				break;
			case Statics.NETWORK_OUT_INDEX:
				features.setNetworkOut(data);
				break;
			case Statics.DISK_READ_INDEX:
				features.setDiskRead(data);
				break;
			case Statics.DISK_WRITE_INDEX:
				features.setDiskWrite(data);
				break;
			case Statics.DISK_SPACE_INDEX:
				features.setDiskUtil(data);
				break;
			case Statics.MEMORY_UTILIZATION_INEDEX:
				features.setMemoryUtil(data);
				break;
			case Statics.SWAP_UTILIZATION_INDEX:
				features.setSwapUtil(data);
				break;
			}
		}
		return features;
	}

	/**
	 * 取得可能な Metrics のリスト
	 * 
	 * @param nameSpace
	 * @param metricName
	 * @return
	 */
	public Metrics getMetrics(String nameSpace, String metricName) {
		ListMetricsRequest request = new ListMetricsRequest();
		request.setNamespace(nameSpace);
		request.setMetricName(metricName);
		ListMetricsResult list = mClient.listMetrics(request);
		Metrics metrics = new Metrics();
		list.getMetrics()
				.stream()
				.forEach(
						(m) -> {
							System.out
									.printf("metricsName=%s, namespace=%s, dimension=%s%n",
											m.getMetricName(),
											m.getNamespace(), m.getDimensions());

							metrics.setMetricName(m.getMetricName());
							metrics.setNameSpace(m.getNamespace());
							metrics.setDimensions(m.getDimensions());
						});
		return metrics;
	}

	/**
	 * 最も新しいデータの取得
	 * 
	 * @param nameSpace
	 * @param metricName
	 * @param dimensions
	 * @return
	 */
	public double getMetricsStatistics(String nameSpace, String metricName,
			List<Dimension> dimensions) {
		GetMetricStatisticsRequest request = new GetMetricStatisticsRequest();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.HOUR, -1);
		request.setStartTime(c.getTime());
		request.setEndTime(new Date());
		request.setNamespace(nameSpace);
		request.setMetricName(metricName);
		request.setPeriod(60);
		request.setDimensions(dimensions);

		Collection<String> statistics = new ArrayList<>();
		statistics.add("Average");
		request.setStatistics(statistics);

		GetMetricStatisticsResult result = mClient.getMetricStatistics(request);
		System.out.println(result.getLabel());
		List<Datapoint> dataPoints = result.getDatapoints();

		Collections.sort(dataPoints, new DataPointComparator());

		dataPoints.stream().forEach(
				(p) -> {
					Date timeStamp = p.getTimestamp();
					Double avg = p.getAverage();

					long timeStampLong = timeStamp.getTime();

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
					simpleDateFormat.applyPattern("YYYY:MM:dd hh:mm:ss");

					System.out.println(simpleDateFormat.format(timeStamp
							.getTime()) + " : " + avg);
				});

		// 最新のデータを返す
		if(dataPoints.size() == 0){
			return -1;
		}
		
		return dataPoints.get(dataPoints.size() - 1).getAverage();
	}
}
