package jp.masazdream.kip.server.anomalydetector;

import java.util.List;

import com.amazonaws.services.cloudwatch.model.Dimension;

public class Metrics {
	private String metricName;

	private String nameSpace;

	private List<Dimension> dimensions;

	// auto generate getter and setter

	public String getMetricName() {
		return metricName;
	}

	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}

	public List<Dimension> getDimensions() {
		return dimensions;
	}

	public void setDimensions(List<Dimension> dimensions) {
		this.dimensions = dimensions;
	}

}
