package jp.masazdream.kip.server.anomalydetector.math;

public class Statistical {
	public static double sigmoid(double z) {
		return 1 / (1 + Math.exp(-z));
	}
}
