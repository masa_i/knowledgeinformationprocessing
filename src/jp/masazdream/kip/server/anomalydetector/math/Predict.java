package jp.masazdream.kip.server.anomalydetector.math;

public class Predict {
	// return plob
	public static double getProb(double[] features, double[] theta) {
		int featureLength = features.length;
		int thetaLength = theta.length;
		if (featureLength != thetaLength) {
			return -1;
		}
		double sum = 0;
		for (int i = 0; i < featureLength; i++) {
			sum += features[i] * theta[i];
		}
		return Statistical.sigmoid(sum);
	}

	// return binayClassfiy 1:positive, 0:negative
	public static int getBinaryClassify(double prob) {
		if (prob >= 0.5) {
			return 1;
		} else {
			return 0;
		}
	}
}
