package jp.masazdream.kip.server.anomalydetector.util;

import java.util.Comparator;

import com.amazonaws.services.cloudwatch.model.Datapoint;

public class DataPointComparator implements Comparator<Datapoint> {

	@Override
	public int compare(Datapoint o1, Datapoint o2) {

		return (int) (o1.getTimestamp().getTime() - o2.getTimestamp().getTime());
	}

}
