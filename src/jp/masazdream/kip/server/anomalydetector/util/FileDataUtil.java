package jp.masazdream.kip.server.anomalydetector.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jp.masazdream.kip.server.anomalydetector.Features;

public class FileDataUtil {

	public static void saveToFile(String filePath, Features features) {
		StringBuilder sb = new StringBuilder();
		sb.append(System.currentTimeMillis());
		sb.append("\t");
		sb.append(features.getCpuUtil());
		sb.append("\t");
		sb.append(features.getNetworkIn());
		sb.append("\t");
		sb.append(features.getNetworkOut());
		sb.append("\t");
		sb.append(features.getDiskRead());
		sb.append("\t");
		sb.append(features.getDiskWrite());
		sb.append("\t");
		sb.append(features.getDiskUtil());
		sb.append("\t");
		sb.append(features.getMemoryUtil());
		sb.append("\t");
		sb.append(features.getSwapUtil());
		sb.append("\r\n");
		try {
			FileWriter fw = new FileWriter(filePath, true);
			PrintWriter pw = new PrintWriter(new BufferedWriter(fw));
			pw.print(sb.toString());
			pw.close();
		} catch (IOException e) {
			System.out.println("[error]" + filePath + ", " + e.getMessage());
		}
		System.out.println("[file output finish]" + filePath);
	}

	public static ArrayList<String[]> getArrayListFromFile(String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			return null;
		}
		ArrayList<String[]> fileLines = new ArrayList<String[]>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] lineSplitTab = line.split("\t");
				fileLines.add(lineSplitTab);
			}
			br.close();
		} catch (Exception e) {
			System.out.println("[error] errata file." + filePath + ". "
					+ e.getMessage());
		}
		return fileLines;
	}
}
