package jp.masazdream.kip.server.anomalydetector.statics;

public class Statics {

	public final static String ACCESS_KEY = "AKIAISKY65CMIWBQQRBQ";
	public final static String SECRET_ACCESS_KEY = "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";
	public final static String MONITORING_END_POINT = "monitoring.ap-northeast-1.amazonaws.com";
	public final static String INSTANCE_ID = "i-bbd582bd";
	public final static String FILE_SYSTEM = "/dev/xvda1";
	public final static String MOUNT_PATH = "/";

	// index
	public final static int CPU_METRIC_INDEX = 0;
	public final static int NETWORK_IN_INDEX = 1;
	public final static int NETWORK_OUT_INDEX = 2;
	public final static int DISK_READ_INDEX = 3;
	public final static int DISK_WRITE_INDEX = 4;
	public final static int DISK_SPACE_INDEX = 5;
	public final static int MEMORY_UTILIZATION_INEDEX = 6;
	public final static int SWAP_UTILIZATION_INDEX = 7;

	public final static String[] METRIC_NAME_SPACES = {
			Statics.CPU_METRIC_NAME_SPACE,
			Statics.NETWORK_IN_METRIC_NAME_SPACE,
			Statics.NETWORK_OUT_METRIC_NAME_SPACE,
			Statics.DISK_READ_METRIC_NAME_SPACE,
			Statics.DISK_WRITE_METRIC_NAME_SPACE,
			Statics.DISK_SPACE_METRIC_NAME_SPACE,
			Statics.MEMORY_UTILIZATION_METRIC_NAME_SPACE,
			Statics.SWAP_UTILIZATION_METRIC_NAME_SPACE };

	public final static String[] METRIC_NAMES = { Statics.CPU_METRIC_NAME,
			Statics.NETWORK_IN_METRIC_NAME, Statics.NETWORK_OUT_METRIC_NAME,
			Statics.DISK_READ_METRIC_NAME, Statics.DISK_WRITE_METRIC_NAME,
			Statics.DISK_SPACE_METRIC_NAME,
			Statics.MEMORY_UTILIZATION_METRIC_NAME,
			Statics.SWAP_UTILIZATION_METRIC_NAME };

	// CPUUtilization(%)
	public final static String CPU_METRIC_NAME_SPACE = "AWS/EC2";
	public final static String CPU_METRIC_NAME = "CPUUtilization";

	// NewrokIn(Bytes)
	public final static String NETWORK_IN_METRIC_NAME_SPACE = "AWS/EC2";
	public final static String NETWORK_IN_METRIC_NAME = "NetworkIn";

	// NetworkOut(Bytes)
	public final static String NETWORK_OUT_METRIC_NAME_SPACE = "AWS/EC2";
	public final static String NETWORK_OUT_METRIC_NAME = "NetworkOut";

	// DiskRead(CountPerSecond)
	public final static String DISK_READ_METRIC_NAME_SPACE = "AWS/EC2";
	public final static String DISK_READ_METRIC_NAME = "DiskReadOps";

	// DiskWrite(CountPerSecond)
	public final static String DISK_WRITE_METRIC_NAME_SPACE = "AWS/EC2";
	public final static String DISK_WRITE_METRIC_NAME = "DiskWriteOps";

	// DiskSpace(%)
	public final static String DISK_SPACE_METRIC_NAME_SPACE = "System/Linux";
	public final static String DISK_SPACE_METRIC_NAME = "DiskSpaceUtilization";

	// MemoryUtilization(%)
	public final static String MEMORY_UTILIZATION_METRIC_NAME_SPACE = "System/Linux";
	public final static String MEMORY_UTILIZATION_METRIC_NAME = "MemoryUtilization";

	// SwapUtilization(%)
	public final static String SWAP_UTILIZATION_METRIC_NAME_SPACE = "System/Linux";
	public final static String SWAP_UTILIZATION_METRIC_NAME = "SwapUtilization";

}
