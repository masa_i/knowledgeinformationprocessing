package jp.masazdream.kip.matching;

/***
Matching Program written



変数:前に？をつける．  

Examle:
% Matching "Takayuki" "Takayuki"
true

% Matching "Takayuki" "Takoyuki"
false

% Matching "?x am Takayuki" "I am Takayuki"
?x = I .

% Matching "?x is ?x" "a is b"
false

% Matching "?x is ?x" "a is a"
?x = a .


Mating は，パターン表現と通常表現とを比較して，通常表現が
パターン表現の例であるかどうかを調べる．
Unify は，ユニフィケーション照合アルゴリズムを実現し，
パターン表現を比較して矛盾のない代入によって同一と判断
できるかどうかを調べる．

***/

/**
* マッチングのプログラム
* 
*/
public class Matching {
  public static void main(String arg[]){
	if(arg.length != 2){
	    System.out.println("Usgae : % Matching [string1] [string2]");
	}
	System.out.println((new Matcher()).matching(arg[0],arg[1]));
  }
}

