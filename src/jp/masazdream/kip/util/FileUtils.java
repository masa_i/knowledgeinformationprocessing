package jp.masazdream.kip.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/***
 * ファイルを扱うクラス
 * 
 * @author masahiro
 * 
 */
public class FileUtils {
	/**
	 * ファイルをロードする
	 * 
	 * @param filePath
	 *            ファイルパス
	 * 
	 * @return　ファイルの各行のリスト、null: ファイルが無い場合
	 * @throws Exception
	 *             ファイル処理中のエラー
	 */
	public static ArrayList<String> loadFile(String filePath) throws Exception {
		File file = new File(filePath);

		if (!file.exists()) {
			// ファイルが無い場合
			return null;
		}

		ArrayList<String> fileLines = new ArrayList<String>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = br.readLine()) != null) {
				fileLines.add(line);
			}

		} catch (Exception e) {
			System.out.println("[error] load file. " + file.getPath() + ". "
					+ e.getMessage());
			throw e;
		} finally {
			// バッファのクローズ
			try {
				br.close();
			} catch (Exception e) {
				System.out.println("[error] close file. " + file.getPath()
						+ ". " + e.getMessage());
				throw e;
			}
		}
		return fileLines;
	}
}
