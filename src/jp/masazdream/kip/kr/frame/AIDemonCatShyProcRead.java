package jp.masazdream.kip.kr.frame;

/*
 AIDemonProcReadTest.java
 すべての種類のデモン手続きのスーパークラス

 when-read procedure は，スロット値を Enumeration として
 返さなけらばならない
 */

import java.util.*;

class AIDemonCatShyProcRead extends AIDemonProc {

	public Object eval(AIFrameSystem inFrameSystem, AIFrame inFrame,
			String inSlotName, Enumeration inSlotValues, Object inOpts) {
		Object sex = inFrame.readSlotValue(inFrameSystem, "sex", false);
		if (sex instanceof String) {
			if (((String) sex).equals("male")) {
				return AIFrame.makeEnum("shy");
			} else if (((String) sex).equals("female")) {
				return AIFrame.makeEnum("notshy");
			}
		}
		return null;
	}

}