package jp.masazdream.kip.kr.frame;

import java.util.Scanner;

/*
 Example.java

 */

public class MyExample {

	public static void main(String args[]) {
		System.out.println("MyFrame");

		// フレームシステムの初期化
		AIFrameSystem fs = new AIFrameSystem();

		// クラスフレーム human の生成
		fs.createClassFrame("human");
		fs.writeSlotValue("human", "sex", "female");
		fs.writeSlotValue("human", "glass", false);
		fs.writeSlotValue("human", "hoby", "none");
		// fs.setWhenRequestedProc("human", "glass", new AIDemonProcReadTest());

		// クラスフレーム pc の生成
		fs.createClassFrame("pc");

		// クラスフレーム cat の生成
		fs.createClassFrame("cat");
		fs.writeSlotValue("cat", "sex", "female");
		fs.setWhenRequestedProc("cat", "shy", new AIDemonCatShyProcRead());

		// インスタンスフレーム masahiro
		fs.createInstanceFrame("human", "masahiro");
		fs.writeSlotValue("masahiro", "sex", "male");
		fs.writeSlotValue("masahiro", "glass", true);
		fs.writeSlotValue("masahiro", "hobby", "guitar");
		fs.writeSlotValue("masahiro", "like", "vaio z");

		// インスタンスフレーム asako
		fs.createInstanceFrame("human", "asako");
		fs.writeSlotValue("asako", "sex", "female");
		fs.writeSlotValue("asako", "hobby", "sing");
		fs.writeSlotValue("asako", "like", "conan");

		// インスタンスフレーム conan
		fs.createInstanceFrame("cat", "conan");
		fs.writeSlotValue("conan", "sex", "male");

		// インスタンスフレーム vaio
		fs.createInstanceFrame("pc", "vaio");

		// 出力human masahiro
		System.out.println("----------------------------------------");
		System.out.println("masahiro ===================");
		System.out.println(fs.readSlotValue("masahiro", "sex", false));
		System.out.println(fs.readSlotValue("masahiro", "glass", false));
		System.out.println(fs.readSlotValue("masahiro", "hoby", false));
		System.out.println(fs.readSlotValue("masahiro", "like", false));

		// 出力human asako
		System.out.println("asako ===================");
		System.out.println(fs.readSlotValue("asako", "sex", false));
		System.out.println(fs.readSlotValue("asako", "glass", false));
		System.out.println(fs.readSlotValue("asako", "hoby", false));
		System.out.println(fs.readSlotValue("asako", "like", false));

		// 出力cat
		System.out.println("conan ===================");
		System.out.println(fs.readSlotValue("conan", "sex", false));
		System.out.println(fs.readSlotValue("conan", "shy", false));
		System.out.println("----------------------------------------");

		System.out.println("検索ワードを入力してください。");
		System.out.println("<masahiro or asako> <sex or like or hobby>");
		System.out.println("終了: q");

		Scanner scan = new Scanner(System.in);

		boolean seaching = true;
		while (seaching) {
			String str1 = scan.next();
			if(str1 == null || str1.equals("")){
				continue;
			}
			if(str1.equals("q")){
				break;
			}
			System.out.println(str1);
			String str2 = scan.next();
			System.out.println(str2);
			System.out.println(fs.readSlotValue(str1, str2, false));
		}
	}

}