package jp.masazdream.kip.kr.semanticnet;

import java.util.Vector;

public class MyExample {
	public static void main(String args[]) {
		myNetwork();
	}

	/**
	 * 自分自身のセマンティックネットワークとクエリ
	 */
	public static void myNetwork() {
		SemanticNet sn = new SemanticNet();

		// 今井はSPRIXの社員である
		sn.addLink(new Link("is-a", "imai", "SPRIX-member", sn));

		// SPRIXの社員は会社員である
		sn.addLink(new Link("is-a", "SPRIX-member", "company-member", sn));

		// 今井は帝京大学大学院の学生である
		sn.addLink(new Link("is-a", "imai", "TEIKYO-univ-posg-student", sn));

		// 帝京大学大学院の学生は学生である
		sn.addLink(new Link("is-a", "TEIKYO-univ-posg-student", "student", sn));

		// 今井はギターを持っている
		sn.addLink(new Link("has-a", "imai", "guiter", sn));

		// ギターは楽器
		sn.addLink(new Link("is-a", "guiter", "instrument", sn));

		// 上司はスプリックスの社員である
		sn.addLink(new Link("is-a", "boss", "SPRIX-member", sn));

		// 今井はvaioを持っている
		sn.addLink(new Link("has-a", "imai", "vaio", sn));

		// vaioはPCである
		sn.addLink(new Link("is-a", "vaio", "pc", sn));

		// 今井はmonsterが好きである
		sn.addLink(new Link("like", "imai", "monster", sn));

		// monsterはエナジードリンクである
		sn.addLink(new Link("is-a", "monster", "enegy-drink", sn));

		sn.printLinks();
		sn.printNodes();

		Vector query = new Vector();
		query.addElement(new Link("like", "?y", "monster"));
		query.addElement(new Link("is-a", "?y", "company-member"));
		query.addElement(new Link("is-a", "?y", "student"));
		query.addElement(new Link("has-a", "?y", "guiter"));
		sn.query(query);
	}
}
