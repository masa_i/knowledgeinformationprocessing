package jp.masazdream.kip.kr.semanticnet;

import java.util.*;

class Node {
    String mName;

    // 自分から出ていくリンク
    Vector mDepartFromMeLinks;
    // 自分に入ってくるリンク
    Vector mArriveAtMeLinks;

    Node(String theName){
	mName = theName;
	mDepartFromMeLinks = new Vector();
	mArriveAtMeLinks   = new Vector();
    }

    public Vector getISATails(){
	Vector isaTails = new Vector();
	for(int i = 0 ; i < mArriveAtMeLinks.size() ; i++){
	    Link theLink = (Link)mArriveAtMeLinks.elementAt(i);
	    if("is-a".equals(theLink.getLabel())){
		isaTails.addElement(theLink.getTail());
	    }
	}
	return isaTails;
    }

    public void addDepartFromMeLinks(Link theLink){
	mDepartFromMeLinks.addElement(theLink);
    }

    public Vector getDepartFromMeLinks(){
	return mDepartFromMeLinks;
    }

    public void addArriveAtMeLinks(Link theLink){
	mArriveAtMeLinks.addElement(theLink);
    }
    
    public Vector getArriveAtMeLinks(){
	return mArriveAtMeLinks;
    }

    public String getName(){
	return mName;
    }

    public String toString(){
	return mName;
    }
}
