package jp.masazdream.kip.kr.semanticnet;

import java.util.*;

public class Link {
	String mLabel;
	Node mTail;
	Node mHead;
	boolean mInheritance;

	Link(String theLabel, String theTail, String theHead, SemanticNet sn) {
		mLabel = theLabel;
		Hashtable<String, Node> nodesNameTable = sn.getNodesNameTable();
		Vector nodes = sn.getNodes();

		mTail = (Node) nodesNameTable.get(theTail);
		if (mTail == null) {
			mTail = new Node(theTail);
			nodes.addElement(mTail);
			nodesNameTable.put(theTail, mTail);
		}

		mHead = (Node) nodesNameTable.get(theHead);
		if (mHead == null) {
			mHead = new Node(theHead);
			nodes.addElement(mHead);
			nodesNameTable.put(theHead, mHead);
		}
		mInheritance = false;
	}

	// For constructing query.
	Link(String theLabel, String theTail, String theHead) {
		mLabel = theLabel;
		mTail = new Node(theTail);
		mHead = new Node(theHead);
		mInheritance = false;
	}

	public void setInheritance(boolean value) {
		mInheritance = value;
	}

	public Node getTail() {
		return mTail;
	}

	public Node getHead() {
		return mHead;
	}

	public String getLabel() {
		return mLabel;
	}

	public String getFullName() {
		return mTail.getName() + " " + mLabel + " " + mHead.getName();
	}

	public String toString() {
		String result = mTail.getName() + "  =" + mLabel + "=>  "
				+ mHead.getName();
		if (!mInheritance) {
			return result;
		} else {
			return "( " + result + " )";
		}
	}
}
