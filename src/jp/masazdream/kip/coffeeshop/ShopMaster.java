package jp.masazdream.kip.coffeeshop;

/**
 * CoffeeShop店長クラス
 * 
 * @author masahiro
 *
 */
public class ShopMaster extends Thread{
	private Counter mCounter;
	public ShopMaster(Counter counter){
		mCounter = counter;
	}
	
	public void run(){
		while(true){
			try {
				mCounter.putCoffee();
				// ランダムに数秒待つ
				Thread.sleep((int)(3000 * Math.random()));
			} catch (InterruptedException e) {
				System.out.println("shopmaster failed to put coffee");
			}
		}		
	}
}
