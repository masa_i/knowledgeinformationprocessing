package jp.masazdream.kip.coffeeshop;

/**
 * CoffeeShopの客クラス 
 * 
 * 
 * @author masahiro
 *
 */
public class CoffeeDrinker extends Thread{
	private Counter mCounter;
	
	private String mName;
	
	public CoffeeDrinker(Counter counter, String name){
		mCounter = counter;
		mName = name;
	}
	
	public void run(){
		while(true){
			try {
				mCounter.getCoffee(mName);
				
				Thread.sleep((int)(10000 * Math.random()));
			} catch (InterruptedException e) {
				System.out.println("drinker failed to get coffee");
			}
			
		}
		
	}
	
	
}
