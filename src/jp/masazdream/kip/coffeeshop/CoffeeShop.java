package jp.masazdream.kip.coffeeshop;

/**
 * 課題02
 * 
 * コーヒーショッププログラム
 * 
 * @author masahiro
 *
 */
public class CoffeeShop {
	/**
	 * 起動
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		Counter counter = new Counter();
		ShopMaster master = new ShopMaster(counter);
		CoffeeDrinker coffeeDrinker1 = new CoffeeDrinker(counter, "first");
		CoffeeDrinker coffeeDrinker2 = new CoffeeDrinker(counter, "second");
		CoffeeDrinker coffeeDrinker3 = new CoffeeDrinker(counter, "third");
		
		master.start();
		coffeeDrinker1.start();
		coffeeDrinker2.start();
		coffeeDrinker3.start();
	}
}
