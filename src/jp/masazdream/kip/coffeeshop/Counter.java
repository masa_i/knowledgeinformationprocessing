package jp.masazdream.kip.coffeeshop;

import java.util.ArrayList;

/**
 * CoffeeShopのカウンタークラス
 * 
 */
public class Counter {
	public static final String COFFEE = "Coffee";
	
	ArrayList<String> mCoffees = null;
	
	int mWaitMSec = 100;
	
	public Counter(){
		mCoffees = new ArrayList<>();
	}
	
	public synchronized void getCoffee(String name) throws InterruptedException{
		while(mCoffees.size() == 0){
			// コーヒーが出来上がるまで待つ
			System.out.println(name + " can not drink a coffee!");
			wait();
		}
		mCoffees.remove(0);
		System.out.println(name + " can drink a coffee!");
		
		// コーヒーが4杯になったら、全てのスレッドを起動
		if(mCoffees.size() == 4){
			notifyAll();
		}
	}
	
	public synchronized void putCoffee() throws InterruptedException{
		mCoffees.add(COFFEE);
		if(mCoffees.size() > 4){
			// コーヒーは十分
			System.out.println("enough coffee");
			wait();
		}
		// コーヒーをつくる
		System.out.println("Master made a coffee");
		
		// 1杯目のコーヒーを入れたら、コーヒー待ちのスレッドを起動
		if(mCoffees.size() == 1){
			notify();
		}
	}
	
}
