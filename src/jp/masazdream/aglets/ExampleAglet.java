package jp.masazdream.aglets;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.ibm.aglet.Aglet;
import com.ibm.aglet.RequestRefusedException;
import com.ibm.aglet.event.MobilityAdapter;
import com.ibm.aglet.event.MobilityEvent;

public class ExampleAglet extends Aglet {
	ExampleMobilityAdapter ma = null;
	boolean remote = false;

	public void onCreation(Object init) {
		System.out.println("I am created.");

		// モビリティアダプタを生成する
		ma = new ExampleMobilityAdapter(this); // ★★
		// モビリティアダプタを加える
		addMobilityListener(ma); // ★★★

		URL destination = null;
		try {
			destination = new URL("atp://masahiro:9001");
		} catch (MalformedURLException mue) {
			System.out.println(mue);
		}
		try {
			dispatch(destination);
		} catch (RequestRefusedException rre) {
			System.out.println(rre);
		} catch (IOException ie) {
			System.out.println(ie);
		}
	}

	public void run() {
		System.out.println("Hello");
	}

	public void onDisposing() {
		System.out.println("Oh! Do you kil Me!?!?!?!?");
	}
}

class ExampleMobilityAdapter extends MobilityAdapter {
	ExampleAglet aglet = null;

	ExampleMobilityAdapter(ExampleAglet theAglet) {
		aglet = theAglet;
	}

	public void onDispatching(MobilityEvent e) {
		System.out.println("I will go!");
		if (aglet.remote) {
			aglet.remote = false;
		} else {
			aglet.remote = true;
		}
	}

	public void onArrival(MobilityEvent e) {
		if (aglet.remote) {
			try {
				// wagnerで一休み
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
			}
			URL destination = null;
			try {
				destination = new URL("atp://tornado.ics.nitech.ac.jp:9001");
			} catch (MalformedURLException mue) {
				System.out.println(mue);
			}
			try {
				aglet.dispatch(destination);
			} catch (RequestRefusedException rre) {
				System.out.println(rre);
			} catch (IOException ie) {
				System.out.println(ie);
			}
		}
	}
}