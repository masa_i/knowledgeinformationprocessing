%load data
data=load('serverFeatures.csv')
x=data(:,[2:end]);
y=data(:,1);
positive = find(y == 1);
negative = find(y == 0);

%normalize
xn=[ones(size(x))];
for i = 1:size(x, 2)
  maxf(i) = max(x(:,i))
  xn(:,i) = x(:,i) ./ max(x(:,i));
end

%save max
save maxf.mat maxf

%bias
[m,n]=size(xn);
xf = [ones(m,1) xn];

%parameters
initial_theta = zeros(size(xf, 2), 1);

%train parameters
options = optimset('GradObj', 'on', 'MaxIter', 400);

%train
[cost, grad]=costFunction(initial_theta, xf, y);
[theta, cost]=fminunc(@(t)(costFunction(t, xf, y)), initial_theta, options);


%validate
validate=load('validateFeatures.csv');
XV=validate(:,[2:end]);

%validate normalize
XVN = [ones(size(XV))];
for i = 1:size(maxf,2)
  XVN(:,i) = XV(:,i) ./ maxf(i);
end

yv=validate(:,1);
[m,n]=size(XVN);
XFV=[ones(m,1) XVN];

pv = predict(theta, XFV);

fprintf('Validate Accuracy: %f\n', mean(double(pv == yv)) * 100);
