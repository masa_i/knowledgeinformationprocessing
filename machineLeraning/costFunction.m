function [J, grad] = costFunction(theta, X, y)
m = length(y); % number of training examples

J = 0;
grad = zeros(size(theta));

T_J = 0;
for i=1:m
  T_J += -y(i)*log(sigmoid(X(i,:) * theta)) - (1-y(i))*log(1 - sigmoid(X(i,:) * theta));
endfor
J = (1/m) * T_J;

grad_tmp = zeros(size(theta));
n = length(grad_tmp);
for j=1:n
  for i=1:m
    grad_tmp(j) += (sigmoid(X(i,:) * theta) - y(i)) * X(i, j);
  endfor
endfor

grad = (1/m) * grad_tmp;

end
